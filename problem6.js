// 6. Find the average salary of based on country. ( Groupd it based on country and then find the average ).
const convertSalaryToNumber = require("./problem2.js");


const findAverageSalaryByCountry = (employeeDetails) => {
    if(Array.isArray(employeeDetails)){
        employeeDetails = convertSalaryToNumber(employeeDetails);
        let averageSalaryByCountry = employeeDetails.reduce((averageSalaryByCountry, employee) => {
            if(!averageSalaryByCountry[employee.location]){
                averageSalaryByCountry[employee.location] = {sum: employee.salary, count: 1};
            }
            else{
                averageSalaryByCountry[employee.location].sum += employee.salary;
                averageSalaryByCountry[employee.location].count += 1;
            }
            return averageSalaryByCountry;
        },{});

        // We can use HOFs(Object Methods) here as well. I will modify it after learning Object Methods.
        // for(key in averageSalaryByCountry){
        //     averageSalaryByCountry[key] = averageSalaryByCountry[key].sum / averageSalaryByCountry[key].count;
        // }

        // Using HOFs
        averageSalaryByCountry = Object.fromEntries(Object.entries(averageSalaryByCountry).map((countrySalary) => {
            countrySalary[1]=(countrySalary[1].sum/countrySalary[1].count);
            return countrySalary;
        }));


        return averageSalaryByCountry;
    }
    else{
        [];
    }
}

module.exports = findAverageSalaryByCountry;