// 3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)
const convertSalaryToNumber = require("./problem2.js");


const modifySalary = (employeeDetails) => {
    if(Array.isArray(employeeDetails)){
        employeeDetails = convertSalaryToNumber(employeeDetails);
        return employeeDetails.map((employee) => {
            return {
                ...employee,
                salary: employee.salary *= 10000
            }
        });
    }
    else{
        return [];
    }
}

module.exports = modifySalary;