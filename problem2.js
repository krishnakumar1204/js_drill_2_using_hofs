// 2. Convert all the salary values into proper numbers instead of strings.


const convertSalaryToNumber = (employeeDetails) => {
    if(Array.isArray(employeeDetails)){
        return employeeDetails.map((employee) => {
            return{
                ...employee,
                salary : Number(employee.salary.replace('$', ''))
            }
        });
    }
    else{
        return [];
    }
}

module.exports = convertSalaryToNumber;