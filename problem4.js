// 4. Find the sum of all salaries.
const convertSalaryToNumber = require("./problem2.js");


const findSumOfSalary = (employeeDetails) => {
    if(Array.isArray(employeeDetails)){
        employeeDetails = convertSalaryToNumber(employeeDetails);
        return employeeDetails.reduce((sum, curr) => sum+curr.salary, 0);
    }
    else{
        return null;
    }
}

module.exports = findSumOfSalary;