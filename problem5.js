// 5. Find the sum of all salaries based on country. ( Group it based on country and then find the sum ).

const convertSalaryToNumber = require("./problem2.js");

const groupSalaryByCountry = (employeeDetails) => {
    if(Array.isArray(employeeDetails)){
        employeeDetails = convertSalaryToNumber(employeeDetails);
        return employeeDetails.reduce((groupedByCountry, curr) => {
            if(!groupedByCountry[curr.location]){
                groupedByCountry[curr.location] = curr.salary;
            }
            else{
                groupedByCountry[curr.location] += curr.salary
            }
            return groupedByCountry;
        }, {});
    }
    else{
        return [];
    }
}

module.exports = groupSalaryByCountry;