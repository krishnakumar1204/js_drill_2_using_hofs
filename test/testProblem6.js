const employeeDetails = require("../data");
const findAverageSalaryByCountry = require("../problem6");

try {
    let averageSalaryByCountry = findAverageSalaryByCountry(employeeDetails);
    console.log(averageSalaryByCountry);
} catch (error) {
    console.log("Something went wrong");
}