const employeeDetails = require("../data");
const modifySalary = require("../problem3");


try {
    let salaryModifiedDetails = modifySalary(employeeDetails);
    console.log(salaryModifiedDetails);
} catch (error) {
    console.log("Something went wrong");
}