const employeeDetails = require("../data");
const convertSalaryToNumber = require("../problem2");


try {
    let convertedSalaryDetails = convertSalaryToNumber(employeeDetails);
    console.log(convertedSalaryDetails);
} catch (error) {
    console.log("Something went wrong");
}