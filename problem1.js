// 1. Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )


const findWebDevelopers = (employeeDetails) => {
    if(Array.isArray(employeeDetails)){
        let webDevelopers = employeeDetails.filter((employee) => employee.job.includes("Web Developer"));
        return webDevelopers;
    }
    else{
        return [];
    }
}

module.exports = findWebDevelopers;